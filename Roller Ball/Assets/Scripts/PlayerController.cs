﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody _rb;

    public int speed;

    public Text scoretext;
    private int count;

	// Use this for initialization
	void Start () {
        _rb = GetComponent<Rigidbody>();

        count = 0;
	}

    private void FixedUpdate()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        _rb.AddForce(movement * speed);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup") )
        {
            other.gameObject.SetActive(false);

            count += 1;

            scoretext.text = "Count: " + count.ToString();
        }
    }
}
